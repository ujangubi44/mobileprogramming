//Mengubah fungsi menjadi fungsi arrow
var golden = () => {
    console.log("This is Golden")
}
golden();

console.log("--------------------------------------------------");

//Sederhanakan menjadi Object literal di ES6
newFunction = (firstName, lastName) => {
    firstName
    lastName
    return {
        fullName() {
            return console.log(firstName + " " + lastName)
        }
    }
}
newFunction("William", "Imoh").fullName();

console.log("--------------------------------------------------");

//Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation)

console.log("--------------------------------------------------");

//Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west, ...east];
console.log(combinedArray)

console.log("--------------------------------------------------");

//Template Literal 
const planet = 'earth'
const view = 'glass'
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod empor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before);

console.log("--------------------------------------------------");